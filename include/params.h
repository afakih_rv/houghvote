// Feature type 
#define _FEAT_TYPE 0 // 0 :hessian-affine, 1: harlap 
#define _DESC_TYPE 0
#define _DISTANT_TYPE 0
#define _NMAXORI 3
#define _PATCHSIZE 31
#define _FEATURESCALE 2
#define _BESTIMATEORIENTATION 1
#define _REGION_TYPE 0 // 0 : affine region
#define _THRES_DIST  0.5
#define _KNN 5
#define _RADIUS 5
#define _BMATCHDISTRIBUTION 1 
#define _REDUNDANCY_THRES  3
#define _BFILTERMATCH 1 
#define _BRESAMPLEPTCH 1
