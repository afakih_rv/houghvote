#ifndef _H_HOUGHVOTE
#define _H_HOUGHVOTE

#include <cstdlib>
#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <string> 

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>

#include "imageobj.h"
#include "utility.h"
#include "svm.h"
using namespace cv;
using namespace std;

class HoughVote{
public:
  HoughVote(ImageObj&);
  ~HoughVote(){};
  void compute_matchList();
  void do_hough_voting();
  void draw_match(int);
  void draw_inliers(const Mat&);

  ImageObj& img_pair_;  
private:
  float find_sigma(const Mat&);
  Mat kernel_density_estimation(const Mat&, float, int);
  Mat create_error_table(int, const multimap<int,int>&, const Mat&);
  Mat make_feat_selection(const Mat&, int , const ImageObj&, vector<EllipseRegion>&);
  int choose_max_density(int, const Mat&,  int, const multimap<int,int>&, float&); 
  void inlier_calssification(const Mat&);
  void read_problem_mat(const Mat&);
  vector<int> predict(const Mat&);
  void setup_parameters(float);

  Mat distance_mat_;
  Mat error_table_, m_error_table_;
  Mat affMat1_,  affMat2_; 
  Mat inlier_mat_; 

};

#endif 