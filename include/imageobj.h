#ifndef _H_IMAGEOBJ
#define _H_IMAGEOBJ

#define _USE_MATH_DEFINES
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <fstream>
#include <cmath> 
#include <algorithm>
#include <functional> 
#include <sstream>
#include <ctime>
#include <chrono>
#include <iterator>
#include <omp.h>
#include <iomanip>
//#include <Eigen/Dense>
//#include <unsupported/Eigen/MatrixFunctions>
//using Eigen::MatrixXd;

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/nonfree/nonfree.hpp>
//#include <opencv2/core/eigen.hpp>

#include "params.h"
#include "utility.h"
#include "chi2float.h"
#include "vlfeat.h"

using namespace std;
using namespace cv;


struct EllipseRegion {
  float ellipse_u, ellipse_v, ellipse_a, ellipse_b, ellipse_c, ellipse_ori;  
  EllipseRegion(float m_pos_u, float m_pos_v, float m_pos_a, float m_pos_b, float m_pos_c, float m_pos_ori = 0): ellipse_u(m_pos_u), ellipse_v(m_pos_v), ellipse_a(m_pos_a), ellipse_b(m_pos_b), ellipse_c(m_pos_c), ellipse_ori(m_pos_ori) 
  {}

  inline bool operator == ( const EllipseRegion& rhs) {
    bool res = (this->ellipse_u == rhs.ellipse_u) && (this->ellipse_v == rhs.ellipse_v) && (this->ellipse_a == rhs.ellipse_a) && (this->ellipse_b == rhs.ellipse_b) && (this->ellipse_c == rhs.ellipse_c) ;
    return res ;
  }
  // copy constructor
  EllipseRegion (const EllipseRegion & rhs){
    ellipse_u    = rhs.ellipse_u;
    ellipse_v    = rhs.ellipse_v;
    ellipse_a    = rhs.ellipse_a;
    ellipse_b    = rhs.ellipse_b;
    ellipse_c    = rhs.ellipse_c;
    ellipse_ori = rhs.ellipse_ori;
  }
  // operator overloading: assignment operator
  inline EllipseRegion & operator= ( const EllipseRegion & rhs ){
    ellipse_u = rhs.ellipse_u;
    ellipse_v  = rhs.ellipse_v;
    ellipse_a = rhs.ellipse_a;
    ellipse_b = rhs.ellipse_b;
    ellipse_c  = rhs.ellipse_c;
    ellipse_ori  = rhs.ellipse_ori;
    return *this;   
  } 
  void printEllipse(){
    cout<<"[u,v,a,b,c,ori]: "<<this->ellipse_u <<" , " <<this->ellipse_v <<" , " <<this->ellipse_a <<" , " <<this->ellipse_b <<" , " <<this->ellipse_c <<" , " << this->ellipse_ori << endl;
  }

};
enum ConvolutionType{
  /* Return the full convolution, including border*/
  CONVOLUTION_FULL, 
  /* Return only the part that corresponds to the original image*/
  CONVOLUTION_SAME,
  /* Return only the submatrix containing elements that were not influenced by the border*/
  CONVOLUTION_VALID
};
class ImageObj{
public:   
  ImageObj(char* refImgPath, char* tarImgPath): mReferenceImgPath(string(refImgPath)), mTargetImgPath(string(tarImgPath)) {
    initialization();
   }
  ~ImageObj();
  void load_images(); 
  void extract_feature(string, Mat&, vector<Mat>&, vector<EllipseRegion>&, Mat&, vector<int>&); 
  inline void extract_feature_ref(){
    extract_feature(mReferenceImgPath, img_ref, affmatrix_ref, ellipse_corners_ref, feat_desc_ref, feat_label_ref);
    string feature_file_name = mReferenceImgPath  + "_feature.png" ;
    save_features(ellipse_corners_ref, img_ref.clone(), feature_file_name);
  }
  inline void extract_feature_tar(){
    extract_feature(mTargetImgPath, img_tar, affmatrix_tar, ellipse_corners_tar, feat_desc_tar, feat_label_tar);
    string feature_file_name = mTargetImgPath + "_feature.png";
    save_features(ellipse_corners_tar, img_tar.clone(), feature_file_name);
  }
  //void load_feature_from_file(string mImgPath, vector<EllipseRegion>&);
  void load_feature_from_file(string mImgPath, vector<EllipseRegion>&, Mat& );
  inline void load_feature_from_file_ref(){
    load_feature_from_file(mReferenceImgPath, ellipse_corners_ref, feat_desc_ref);
   }
  inline void load_feature_from_file_tar(){
    load_feature_from_file(mTargetImgPath, ellipse_corners_tar, feat_desc_tar); 
  }
  void make_initial_matches();  
  void draw_features( const vector<EllipseRegion>&, Mat& img);
  void save_features(  const vector<EllipseRegion>&, Mat img, string filename);
  void extract_affine_transform(const vector<EllipseRegion>&, Mat&,  vector<Mat>&, vector<EllipseRegion>&, Mat&, vector<int>&);


  vector<Mat> affmatrix_ref, affmatrix_tar; // affine transform
  multimap<int, int>  matchlist;                   // 
  Mat feat_desc_ref, feat_desc_tar;                  // descriptor of reference and target images
  vector<EllipseRegion> ellipse_corners_ref, ellipse_corners_tar;
  vector<int> feat_label_ref, feat_label_tar;
  Mat img_ref, img_tar;         // reference and target images 
private: 
  string mReferenceImgPath, mTargetImgPath; 
  string feat_type_, desc_type_, region_type_, distance_type_;
  
  vector<int> feat_idx_ref, feat_idx_tar;
  vector<float> angle_ref, angle_tar; 
  vector<Mat> feat_patch_ref, feat_patch_tar; 
  vector<float> matchlist_score; 

  float scaling, sz_patch; // scale factor for descriptor, patch size for descriptor 
  int n_max_orientation;
  int knn;    
  float thres_dist, radius_nn; 
  bool b_match_distribution, b_filter_match ; 
  bool b_use_flann;
  bool b_resample_ptch;
  float redundancy_threshold; 

  void initialization();
  void compute_norm_trans_image(const vector<EllipseRegion>&, Mat&, vector<Mat>&, vector<int>&, vector<EllipseRegion>&, vector<float>&); 
  void from_DOG_to_trans(const vector<EllipseRegion>&, vector<Mat>&, vector<int>&, vector<EllipseRegion>&, vector<float>&);  
  void compute_grad_mag(const Mat&, float, Mat&, Mat& ); 
  void meshgrid(const Mat &xgv, const Mat& ygv, Mat &X, Mat& Y);
  void eliminate_matches(vector<int>&);
  Mat generate_descriptor(const vector<Mat>&);
  Mat gensiftdesc_color(const Mat&);
  Mat gensiftdesc(const Mat&);
  void from_DOG_to_ellipse(vector<EllipseRegion>&);
  Mat take_floor(const Mat&);
  Mat take_power(const Mat&, float);
  void take_atan(const Mat&, const Mat&, Mat&); 
  Mat conv2(const Mat&, const Mat&, ConvolutionType); 
  bool normalize_patch(const Mat&, const Mat&, Mat&);
  vector<float> dominant_orientation(Mat&);
  vector<EllipseRegion> purify_feature(const vector<EllipseRegion>&); 
  vector<int> generate_feat_label(const vector<EllipseRegion>&);
  
  Mat compute_L2_distance(const Mat&, const Mat&);
  Mat compute_chi_distance(const Mat&, const Mat&);
  Mat compute_chi_distance_fast(const Mat&, const Mat&) ;
  void take_match_id(const vector<int>, const int, int&, int&);
  vector<int> brute_search_wrapper(const Mat&, const Mat&, float);

};

#endif