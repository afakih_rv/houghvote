#ifndef _H_UTILITY
#define _H_UTILITY

#include <vector>
#include <map>
#include <iostream>
#include <string> 
#include <fstream>
#include <sstream>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>

#include "imageobj.h"
using namespace std;

class EllipseRegion;

void check_max_min_value(const cv::Mat& input_mat);
vector<int> map_to_vector(multimap<int, int>& input, int nMatch);
void print_multi_map(multimap<int, int>&);
void dump_map_to_file(multimap<int,int>& input, string filename);
void dump_floatmat_to_file(cv::Mat& input, string filename);
void read_file_to_map(multimap<int,int>&, string filename);
void read_file_to_ellipse(vector<EllipseRegion>& featureVec, string filename);
void read_file_to_affmatrix(cv::Mat& affMat, string filename);
void read_file_to_errortable(cv::Mat& error_table, string filename);
void dump_ellipse_to_file( vector<EllipseRegion>& featureVec, string filename);
void BruteRSearch(float* , float* , float, int , int, vector<int>* );


// void dump_mat_to_file( cv::Mat& input, string filename);
template <class T>
void dump_mat_to_file( cv::Mat& input, string filename){
  ofstream ofs(filename.c_str(), std::ofstream::out);
  for (int i = 0 ; i < input.rows; i++){
    for (int j = 0 ; j < input.cols ; j++){
      ofs << setw(8) << input.at<T>(i,j) <<setw(8);
    }
    ofs<<endl;
  }
  ofs.close();
}

string type2str(int);

template<typename T> void dump_vector_to_file( vector<T>& input, string filename){
  ofstream ofs(filename.c_str(), std::ofstream::out);
  for (auto v: input){
    ofs <<v<<"\n";
  }
  ofs.close();
}

template<typename T>void read_file_to_vector(vector<T>& input,  string filename){
      input.clear();
      ifstream ifs( filename.c_str(),  std::ifstream::in); 
      if (! ifs.is_open() )
        cout<<"cannot open: "<<filename.c_str() <<endl;
      string str; 
      float value;  
      while ( std::getline(ifs, str) ) {
        stringstream ss; 
        ss << str; 
        ss >> value;
        input.push_back(value);   
      }
      ifs.close();
}

/* sort */
template<typename T> vector<int> sort_index(const vector<T> &v, bool descend = false){
        // initialize original index locations 
        vector<int> idx(v.size()); 
        for (int i = 0 ; i != idx.size() ; i++) {
          idx[i] = i ; 
        }   
        if ( descend == false ){
                // sort indexes based on comparing values in v 
               sort( idx.begin(), idx.end(), [&v](size_t i1, size_t i2) { return v[i1] <= v[i2] ; });
        } else {
                sort( idx.begin(), idx.end(), [&v](size_t i1, size_t i2) { return v[i1] > v[i2] ; });
        }
        return idx ;
}

template<typename T> vector<T> erase_element_from_array_int(const vector<T>& inVec, const vector<int>& index_array){
      vector<T> outVec;
      outVec.reserve(inVec.size()); 
      for ( auto iter = index_array.begin() ; iter != index_array.end() ; iter++){
        outVec.push_back(inVec.at(*iter)); 
      }
      return outVec;
}

/* Erase the out_array[index_array] */ 
template <typename T> vector<T> erase_element_from_array_bool(const vector<T> &inVec, const vector<bool>& index_array){
      vector<T> outVec;
      outVec.reserve( inVec.size());
      int count = 0; 
      for ( auto iter = index_array.begin() ; iter != index_array.end() ; iter++){
        if (*iter == false) {     
          outVec.push_back(inVec.at(count));      
        }
        count++;
      }
      return outVec;
}

template<typename T> multimap<T, T> erase_element_from_map(multimap<T,T>& inMap, const vector<int>& index_array){
  multimap<int, int> outMap;
  int index = 0;
  for ( auto iter = inMap.begin(); iter!= inMap.end() ; iter++, index++ ) {
    auto it = std::find( index_array.begin(),  index_array.end() , index) ;
    if (it != index_array.end() )
      outMap.insert( std::make_pair( iter->first, iter->second) ); 
  }
  return outMap; 
}

/* Set the out_array[index_array] to zero*/ 
template<typename T> void set_vector_index_zero(vector<T>&  out_array, const vector<int>&  index_array){
    for ( vector<int>::const_iterator iter = index_array.begin() ; iter < index_array.end() ; iter++ ) {
      out_array.at(*iter - 1 ) = 0 ; 
    }
}

/* This function find the */
template<typename T> int find_loc_in_multimap(const multimap<T,T>& myMap, T& searchKey) {
      auto it   = myMap.lower_bound(searchKey); 
      int count = std::distance(myMap.begin(), it);
      return count;
}



#endif  