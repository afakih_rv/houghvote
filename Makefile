#CFLAGS = -std=c++11 -Wall -g
CFLAGS = -std=c++11 -O3 -Wall -march=nocona -Iinclude
all:
	g++ -c $(CFLAGS) src/houghvote.cpp  -o obj/houghvote.o
	g++ -c $(CFLAGS) src/imageobj.cpp -o obj/imageobj.o
	g++ -c $(CFLAGS) src/main.cpp -o obj/main.o
	g++ -c $(CFLAGS) src/utility.cpp -o obj/utility.o
	g++ -c $(CFLAGS) src/chi2float.c -o obj/chi2float.o
	g++ -o houghvote obj/houghvote.o obj/imageobj.o obj/main.o obj/utility.o obj/chi2float.o `pkg-config --cflags opencv` `pkg-config --libs opencv` -fopenmp -lvl -L/home/fensi/downloads/libsvm-3.21 -Llib/ -lsvm
run:
	LD_LIBRARY_PATH=/home/fensi/projects/HoughVote/lib/:$(LD_LIBRARY_PATH) ./houghvote ./data/339_nearFront.png ./data/339_curBack.png