#include "houghvote.h"
#include "imageobj.h"

#define Malloc(type,n) (type *)malloc((n)*sizeof(type))
struct svm_parameter param;   // set by parse_command_line
struct svm_problem prob;    // set by read_problem
struct svm_model *model;
struct svm_node *x_space;
struct svm_node *predicted_labels;
int max_nr_attr = 64;

/*   
Find all elements' indices in vector v that equal to candidate_value 
*/
vector<int> find_elements_indices(const vector<int> v , int candidate_value) {

  vector<int> matches ;
  auto i       = v.begin() ; 
  auto end = v.end();   
  while ( i != end ) {
    i = std::find( i, v.end(), candidate_value );
    if (i < end) {
      int idx = std::distance( v.begin() ,i ) ;     
      matches.push_back(idx);
      i++;      
    }
  }
  return matches; 
}

/*
Convert a vector of cv:Mat (3x3) to a matrix 
*/
void vectorMatToMat(vector<Mat>& input, Mat& output ){
  int nFeat = input.size() ; 
  for (int i = 0 ; i < nFeat ; i++) {
    Mat tmp = input.at(i).reshape(1,1);
    tmp.copyTo( output.row( i ) ) ;
  }
}

// Add on Functions
void MatMul(float* MatA, float* MatB, float* MatC, int type) {
    if (type == 1) {
        MatC[0] = MatA[0]*MatB[0]+MatA[1]*MatB[3];
        MatC[1] = MatA[0]*MatB[1]+MatA[1]*MatB[4];
        MatC[2] = MatA[0]*MatB[2]+MatA[1]*MatB[5]+MatA[2];
        MatC[3] = MatA[3]*MatB[0]+MatA[4]*MatB[3];
        MatC[4] = MatA[3]*MatB[1]+MatA[4]*MatB[4];
        MatC[5] = MatA[3]*MatB[2]+MatA[4]*MatB[5]+MatA[5];
    }
    else {
        MatC[0] = MatA[0]*MatB[0]+MatA[1]*MatB[1]+MatA[2];
        MatC[1] = MatA[3]*MatB[0]+MatA[4]*MatB[1]+MatA[5];
    }
}
void MatInv(float* MatA, float* MatB) {
    
    float detA = MatA[0]*MatA[4]-MatA[1]*MatA[3];
    MatB[0]        = MatA[4]/detA;
    MatB[1]        = -MatA[1]/detA;
    MatB[2]        = (MatA[1]*MatA[5]-MatA[2]*MatA[4])/detA;
    MatB[3]        = -MatA[3]/detA;
    MatB[4]        = MatA[0]/detA;
    MatB[5]        = (MatA[2]*MatA[3]-MatA[0]*MatA[5])/detA;
   
}
float MatDet(float* MatA) {
    return MatA[0]*MatA[4]-MatA[1]*MatA[3];
}

void aff_trans_dist(int* pMatch, int nMatch, float* pAff1, int nFeat1, float* pAff2, int nFeat2, float* pDist)
{
    int i,j,idx1i,idx2i,idx1j,idx2j;
    float T1i[6], T2i[6], T12i[6], T21i[6], T1i_inv[6], T2i_inv[6];
    float P1j[2], P2j[2], P1j_tran[2], P2j_tran[2];
    float dist1, dist2;
    
    for(i = 0; i < nMatch*nMatch; i++) pDist[i] = 0;
        
    for(i = 0; i < nMatch; i++)
    {
        idx1i = pMatch[i];
        idx2i = pMatch[i+nMatch];

        T1i[0] = pAff1[idx1i];
        T1i[1] = pAff1[idx1i+1*nFeat1];
        T1i[2] = pAff1[idx1i+2*nFeat1];
        T1i[3] = pAff1[idx1i+3*nFeat1];
        T1i[4] = pAff1[idx1i+4*nFeat1];
        T1i[5] = pAff1[idx1i+5*nFeat1];

        T2i[0] = pAff2[idx2i];
        T2i[1] = pAff2[idx2i+1*nFeat2];
        T2i[2] = pAff2[idx2i+2*nFeat2];
        T2i[3] = pAff2[idx2i+3*nFeat2];
        T2i[4] = pAff2[idx2i+4*nFeat2];
        T2i[5] = pAff2[idx2i+5*nFeat2];        
        
        for(j = 0; j < nMatch; j++)
        {
            if(i == j) continue;   
            idx1j = pMatch[j];
            idx2j = pMatch[j+nMatch];
                   
            P1j[0] = pAff1[idx1j+2*nFeat1]; P1j[1] = pAff1[idx1j+5*nFeat1];
            P2j[0] = pAff2[idx2j+2*nFeat2]; P2j[1] = pAff2[idx2j+5*nFeat2];
           
            MatInv(T1i,T1i_inv); MatInv(T2i,T2i_inv);    
            MatMul(T2i,T1i_inv,T12i,1);
            MatMul(T1i,T2i_inv,T21i,1);
            MatMul(T12i,P1j,P1j_tran,2);
            MatMul(T21i,P2j,P2j_tran,2);
            dist1 = (P1j[0]-P2j_tran[0])*(P1j[0]-P2j_tran[0])+(P1j[1]-P2j_tran[1])*(P1j[1]-P2j_tran[1]);
            dist2 = (P2j[0]-P1j_tran[0])*(P2j[0]-P1j_tran[0])+(P2j[1]-P1j_tran[1])*(P2j[1]-P1j_tran[1]);
            pDist[i+nMatch*j] = (sqrt(dist1)+sqrt(dist2))/2;
        }    
       
    }

    for(j = 0; j < nMatch-1; j++)
        for(i = j+1; i < nMatch; i++)
        {
            pDist[j+nMatch*i] = (pDist[j+nMatch*i]+pDist[i+nMatch*j])/2;
            pDist[i+nMatch*j] = pDist[j+nMatch*i];
        }
 
    return;
}

HoughVote::HoughVote(ImageObj& imgObjInstance) : img_pair_(imgObjInstance) {
  int nMatch      = img_pair_.matchlist.size(); 
  distance_mat_   = Mat( nMatch, nMatch, CV_32F); 
  vector<int> pMatch = map_to_vector( img_pair_.matchlist , nMatch) ; 
  int nFeat1 = img_pair_.affmatrix_ref.size();
  int nFeat2 = img_pair_.affmatrix_tar.size();
  affMat1_.create(nFeat1,9, CV_32F);
  affMat2_.create(nFeat2,9, CV_32F);
  vectorMatToMat( img_pair_.affmatrix_ref, affMat1_);
  vectorMatToMat( img_pair_.affmatrix_tar, affMat2_);
  affMat1_ = affMat1_.t();
  affMat2_ = affMat2_.t(); 
  std::chrono::time_point<std::chrono::system_clock> start, end;
  start             = std::chrono::system_clock::now();
  aff_trans_dist( &pMatch[0],  nMatch,  (float*)affMat1_.data,  nFeat1 ,  (float*)affMat2_.data,  nFeat2 ,  (float*)distance_mat_.data ); 
  end = std::chrono::system_clock::now();
  std::chrono::duration<float> elapsed_seconds = end-start;
  cout<<"elapsed time for aff_trans_dist estimation: " << elapsed_seconds.count() << "s\n";  
}


/* Find the */
float HoughVote::find_sigma(const Mat& distance_mat_){
  Scalar sigma_value;
  // put MAX in the diagonal of distance_mat_   
  Mat tmp = distance_mat_.clone();
  double maxValue, minValue; 
  Point min_loc, max_loc;  
  minMaxLoc(distance_mat_, &minValue, &maxValue, &min_loc, &max_loc);
  tmp.diag().setTo(maxValue);  
  Mat dst(tmp.rows, 1 , CV_32F); 
  reduce(tmp, dst, 1 , CV_REDUCE_MIN); 
  sigma_value =  mean(dst);
  return sigma_value[0];
}

void HoughVote::do_hough_voting(){
      
  int order                                    = 1 ;
  int nInitialMatch                        = img_pair_.matchlist.size(); 
  int nFeat                                    = img_pair_.affmatrix_ref.size();
  float min_sigma                         =  find_sigma(distance_mat_); 
  float kde_sigma                         = pow(2,-1)*min_sigma;
  Mat total_density                       = kernel_density_estimation(distance_mat_, kde_sigma, nInitialMatch) ; 
  error_table_                               = create_error_table(nFeat, img_pair_.matchlist, total_density); 
  vector<EllipseRegion> m_feat_Ref;
  m_error_table_                          = make_feat_selection(error_table_, order, img_pair_,  m_feat_Ref); 
  inlier_calssification(m_error_table_ );
}

void HoughVote::inlier_calssification( const Mat& m_error_table ){

/* */
int nFeat1                = affMat1_.cols; 
int nFeat2                = affMat2_.cols;
Mat matchlist_vote = m_error_table( Range::all(), Range(1,3)) ;
int nMatch             =  matchlist_vote.rows;
matchlist_vote = matchlist_vote.t();
Mat tmp = matchlist_vote.reshape(0, nMatch*2);
vector<int> matchlist_vec( nMatch * 2);
tmp.copyTo(matchlist_vec);

/* construct transformation space */
Mat inlier_dist_mat = Mat( nMatch, nMatch, CV_32F); 
std::chrono::time_point<std::chrono::system_clock> start, end;
start             = std::chrono::system_clock::now();
aff_trans_dist( &matchlist_vec[0],  nMatch,  (float*)affMat1_.data,  nFeat1 ,  (float*)affMat2_.data,  nFeat2 ,  (float*)inlier_dist_mat.data ); 
end = std::chrono::system_clock::now();
std::chrono::duration<float> elapsed_seconds = end-start;
cout<<"elapsed time for aff_trans_dist estimation: " << elapsed_seconds.count() << "s\n";  

/* prepare kernel */
float min_sigma        =  find_sigma(inlier_dist_mat);
float sigma               = pow(2,-1)*min_sigma;

Mat train_data_mat; 
cv::exp( -abs(inlier_dist_mat)/sigma, train_data_mat);
Mat label_mat = Mat::ones( nMatch, 1, CV_32F );
float nu = 0.95;

Mat input_kernel_mat; 
hconcat(label_mat, train_data_mat, input_kernel_mat);
read_problem_mat(input_kernel_mat);
setup_parameters(nu);
model = svm_train(&prob,&param);
vector<int> output_label = predict(input_kernel_mat);

/* release memory */
svm_free_and_destroy_model(&model);
free(predicted_labels);
svm_destroy_param(&param);
free(prob.y);
free(prob.x);
free(x_space);

/* */
Mat m_inlier_error_table; 
for ( unsigned int i = 0; i < output_label.size() ; i++ ){
    if ( output_label[i] == -1)
      continue;
    Mat tmp = ( Mat_<float>(1,3) << m_error_table_.at<float>(i, 0), m_error_table_.at<float>(i, 1),m_error_table_.at<float>(i, 2) );
    m_inlier_error_table.push_back(tmp);  
}

draw_inliers(m_inlier_error_table);

}

Mat HoughVote::kernel_density_estimation(const Mat& s_distance_mat_, float sigma, int totalNum){
  Mat subDistance ;
  exp( -abs(s_distance_mat_)/sigma, subDistance); 
  Mat totalDensity ; 
  reduce(subDistance, totalDensity,1, CV_REDUCE_SUM);
  totalDensity = totalDensity/totalNum;
  return totalDensity; 
}

/* This function compares density of each candidate pairs for each feature
Input: 
Output: [density , mID1, mID2] --> each entry in error_table_  
*/
Mat HoughVote::create_error_table(int nFeat, const multimap<int,int>& matchlist, const Mat& totalDensity ){
  Mat error_table_(nFeat, 3, CV_32F);
  //BUG FIXME 
  for (int feat_id = 0 ; feat_id < nFeat; feat_id++){
    int numElement                 = matchlist.count(feat_id) ; 
    if ( numElement == 0 ){
        error_table_.at<float>(feat_id,0) = 0; 
        error_table_.at<float>(feat_id,1) = 0 ; 
        error_table_.at<float>(feat_id,2) = 0 ; 
       continue;
    }
    int start_index                                = find_loc_in_multimap(matchlist, feat_id);
    if ( start_index < 0 )  continue ;
    float vote_den                            = 0; 
    int vote_idx                                     = choose_max_density(feat_id, totalDensity, start_index, matchlist, vote_den); 
    error_table_.at<float>(feat_id,0) = vote_den; 
    error_table_.at<float>(feat_id,1) = feat_id ; 
    error_table_.at<float>(feat_id,2) = vote_idx ; 
  }
  return error_table_;
}

/* 
This function 
Input: 
Output:  m_error_table_ [ id1, id2 , id3 ] : 
*/ 
Mat HoughVote::make_feat_selection(const Mat& error_table_, int order, const ImageObj& img_pair_, vector<EllipseRegion>& m_feat_Ref){
  
  //find the length of unique feature id in 
  vector<int> featLabel = img_pair_.feat_label_ref; 
  vector<int> tmp           = featLabel ;
  
  auto unique_end         = std::unique(tmp.begin(),  tmp.end() ); 
  int unique_nFeat          = std::distance( tmp.begin(), unique_end  )  ;  
    
  Mat _error_table_ = cv::Mat::zeros(0, 3, CV_32F); 

  for (int canIdx = 0; canIdx < unique_nFeat  ; canIdx++){
    vector<int> keyIndex          = find_elements_indices( featLabel, canIdx ); 
    int startIdx                         = keyIndex.at(0);
    Mat subTable                      =  error_table_(cv::Rect(  0 , keyIndex.at(0) , 1 , keyIndex.size() ) );
    subTable = subTable.t();  
    vector<float> v;
    subTable.row(0).copyTo(v);
    auto result                          = std::max_element( v.begin(), v.end() ) ; 
    int max_candidate_id          =  std::distance( v.begin(), result );  //max density index among all the candidate for each feature 
     //conver to original id in feature space : feat_id
     int feat_id                      =  startIdx + max_candidate_id ;      
     Scalar val = cv::sum(error_table_.row(feat_id));
     if ( val[0]  != 0) {     
         Mat tmp =  ( Mat_<float>(1,3) << error_table_.at<float>(feat_id,0 ), error_table_.at<float> (feat_id,1), error_table_.at<float> (feat_id,2));
         _error_table_.push_back(tmp) ;
     }           
  }
  
  return _error_table_;
}


/* for each feature, choose the matched ID with maximum density
Input:    feaIndex (feature ID), densityVec ( matrix ), matchlist 
Output: idx2j (the matchPair of feaIndex) and vote_den (maximum density ) 
*/
int HoughVote::choose_max_density(int feaIndex, const Mat& densityVec,  int start_index, const multimap<int,int>& matchlist, float& vote_den) {
  float max_density     =  0 ; 
  int idx2j                          = 0 ;
  int numOfElement       = matchlist.count(feaIndex); 
  std::pair <multimap<int,int>::const_iterator, multimap<int,int>::const_iterator > ret =  matchlist.equal_range(feaIndex);
  if ( numOfElement != 0 ) {
    int j = 0 ;
    // for each candidate pairs  of feaIndex, find the one with maximum density 
    for (auto it = ret.first; it != ret.second; ++it, ++j){
      float cur_density = densityVec.at<float>(0, start_index + j); 
      if ( cur_density >= max_density ){
        idx2j               = it->second;
        max_density  = densityVec.at<float>(0, start_index + j);
        vote_den        = max_density; 
      }
    }
  }else{
    cout<<"Can not find feature ["<<feaIndex<<"]."<<endl;
  }
  return idx2j;
}


void HoughVote::draw_match(int nMatch){
  RNG rng(12345);
  Mat canvas;
  int mIdx1, mIdx2;   
  hconcat( img_pair_.img_ref,img_pair_.img_tar, canvas);
  vector<float> density;
  Mat tmp = m_error_table_.t();
  tmp.row(0).copyTo(density);
  vector<int> rank_idx     =  sort_index<float>(density, true) ;         
  for (int i = 0 ; i < nMatch ; i++){
    Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
    mIdx1 = m_error_table_.at<float>(rank_idx.at(i), 1);
    mIdx2 = m_error_table_.at<float>(rank_idx.at(i), 2); 

    Point pt1(  img_pair_.ellipse_corners_ref.at(mIdx1).ellipse_u, img_pair_.ellipse_corners_ref.at(mIdx1).ellipse_v ); 
    Point pt2(  img_pair_.ellipse_corners_tar.at(mIdx2).ellipse_u  + img_pair_.img_ref.cols , img_pair_.ellipse_corners_tar.at(mIdx2).ellipse_v ); 
    circle( canvas,  pt1, 3, color, 2, 8, 0); 
    circle( canvas,  pt2, 3, color, 2, 8, 0); 
    cv::line(canvas, pt1, pt2,  Scalar(0,255,0));
  }
       
  string saveFileName = "Result_" + std::to_string(nMatch) + ".png";
  imwrite(saveFileName.c_str(), canvas);
  imshow( "Match", canvas);
  waitKey(0); 
}

void HoughVote::draw_inliers(const Mat& m_inlier_error_table){
  RNG rng(12345);
  Mat canvas;
  int mIdx1, mIdx2;   
  hconcat( img_pair_.img_ref,img_pair_.img_tar, canvas);
  int nMatch = m_inlier_error_table.rows;  
  for (int i = 0 ; i < nMatch ; i++){
    Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
    mIdx1 = m_inlier_error_table.at<float>(i, 1);
    mIdx2 = m_inlier_error_table.at<float>(i, 2); 
    Point pt1(  img_pair_.ellipse_corners_ref.at(mIdx1).ellipse_u, img_pair_.ellipse_corners_ref.at(mIdx1).ellipse_v ); 
    Point pt2(  img_pair_.ellipse_corners_tar.at(mIdx2).ellipse_u  + img_pair_.img_ref.cols , img_pair_.ellipse_corners_tar.at(mIdx2).ellipse_v ); 
    cv::circle( canvas,  pt1, 3, color, 2, 8, 0); 
    cv::circle( canvas,  pt2, 3, color, 2, 8, 0); 
    cv::line(canvas, pt1, pt2,  Scalar(0,255,0));
  }
       
  string saveFileName = "Result_" + std::to_string(nMatch) + ".png";
  imwrite(saveFileName.c_str(), canvas);
  //imshow( "Match", canvas);
  //waitKey(0); 
}


// read in a problem (in svmlight format)
void HoughVote::read_problem_mat(const Mat& input_mat)
{
  int max_index, inst_max_index, i;
  size_t elements, j;
  
  prob.l      =  input_mat.rows;         //number of training data
  elements =  input_mat.rows * (input_mat.cols + 1);

  prob.y      = Malloc(double,prob.l);  //an array containing their target values
  prob.x      = Malloc(struct svm_node *,prob.l);
  x_space    = Malloc(struct svm_node,elements);
  max_index = 0;
  j=0;

  for(i=0;i< input_mat.rows;i++) //for each instance
  {
    inst_max_index = -1; // strtol gives 0 if wrong format, and precomputed kernel has <index> start from 0  
    prob.x[i]            = &x_space[j];
    prob.y[i]            = (int) input_mat.at<float>(i,0);   
    x_space[j].index = 0;
    x_space[j].value = i+1;
    j++;
    for ( int k = 1;  k < input_mat.cols; k++){
      x_space[j].index = k;  
      inst_max_index = x_space[j].index;
      x_space[j].value = input_mat.at<float>(i, k);
      ++j;
    }
    if(inst_max_index > max_index)
      max_index = inst_max_index;

    x_space[j++].index = -1;
  }

  if(param.gamma == 0 && max_index > 0)
    param.gamma = 1.0/max_index;
}

void HoughVote::setup_parameters(float nu){
  param.svm_type = ONE_CLASS;
  param.kernel_type = PRECOMPUTED;
  param.degree = 3;
  param.gamma = 0;  // 1/num_features
  param.coef0 = 0;
  param.nu = nu;
  param.cache_size = 100;
  param.C = 1;
  param.eps = 1e-3;
  param.p = 0.1;
  param.shrinking = 1;
  param.probability = 0;
  param.nr_weight = 0;
  param.weight_label = NULL;
  param.weight = NULL;
}


vector<int> HoughVote::predict(const Mat& input_mat){

  vector<int> output_label;
  double p_label;
  predicted_labels = (struct svm_node *) realloc(predicted_labels,max_nr_attr*sizeof(struct svm_node));
  for (int i = 0 ; i < input_mat.rows; i++) {
     int inst_max_index = -1;     
     for (int k = 1; k < input_mat.cols ; k++){       
        if(k >=max_nr_attr-1) // need one more for index = -1
        {
          max_nr_attr *= 2;
          predicted_labels = (struct svm_node *) realloc(predicted_labels,max_nr_attr*sizeof(struct svm_node));
        }
          predicted_labels[k-1].index  = k;      
          inst_max_index                 = predicted_labels[k].index;        
          predicted_labels[k-1].value  = input_mat.at<float>(i,k);    
     }
    predicted_labels[input_mat.cols].index = -1;
    p_label = svm_predict(model, predicted_labels);
    output_label.push_back(p_label);
  }
  return output_label;
}