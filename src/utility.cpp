#include "utility.h"

/*  
Convert c++ multimap structure to vector  
*/
vector<int> map_to_vector(multimap<int, int>& input, int nMatch){
  vector<int> matchListIdx(nMatch*2);
  
  int index = 0;
  for ( auto mIter = input.begin(); mIter != input.end() ; mIter++){
    int idx1 = mIter->first; 
    int idx2 = mIter->second;
    matchListIdx[ index ] = idx1; 
    matchListIdx[ index+ nMatch ] = idx2;
    index++;
  }
  return matchListIdx; 
}

/* 
print the element inside the multi-map
*/
void print_multi_map(multimap<int , int>& input){ 
   std::pair<std::multimap<int, int>::iterator,std::multimap<int, int >::iterator> range;
  for (auto v = input.begin() ; v != input.end() ;  v = input.upper_bound(v->first) ) {
    int idx1 = v->first ;
    range = input.equal_range(idx1) ;
    for (auto d = range.first ; d != range.second ; d++){
      cout<< d->first <<", "<< d->second<<endl;;
    }
  }
}

/*
write opencv Mat to file
*/
void dump_floatmat_to_file(cv::Mat& input, string filename){
  ofstream ofs( filename.c_str(), std::ofstream::out); 
  for (int i = 0 ; i < input.rows; i++){
    for (int j = 0 ; j < input.cols; j++){
      ofs<<input.at<float>(i,j) <<"\t";
    }
    ofs<<"\n";
  }
  ofs.close();
}

/*
write std::multimap to file
*/
void dump_map_to_file(multimap<int,int>& input, string filename){
  std::pair<std::multimap<int, int>::iterator,std::multimap<int, int >::iterator> range;
  ofstream ofs(filename.c_str(), std::ofstream::out);
  for (auto v = input.begin() ; v != input.end() ; v = input.upper_bound(v->first)){
    int idx1 = v->first ;
    range = input.equal_range(idx1) ;
    for (auto d = range.first ; d != range.second ; d++){
      ofs<< d->first <<setw(9) <<d->second<<"\n";
    }
  }
  ofs.close();
}

/* 
read .txt file to multimap
*/ 
void read_file_to_map(multimap<int,int>& output, string filename){
  output.clear();
  ifstream ifs(filename.c_str(), std::ifstream::in);
  if (! ifs.is_open()) 
    cout<<"cannot open file."<<endl;
  string str;
  int idx1, idx2;
  while( std::getline(ifs, str) ){
    stringstream ss;
    ss << str; 
    ss >> idx1 >> idx2 ;
    output.insert(std::make_pair( idx1, idx2 ) ); 
  }
  ifs.close();
}

/* 
read .txt file to vector<Ellipse> 
*/
void read_file_to_ellipse( vector<EllipseRegion>& featureVec, string filename){
  featureVec.clear();
  ifstream ifs( filename.c_str(),  std::ifstream::in); 
  if (! ifs.is_open() )
    cout<<"Cannot open file "<<filename.c_str() <<endl;
  string str; 
  float ellipse_u, ellipse_v, ellipse_a, ellipse_b, ellipse_c;  
  while ( std::getline(ifs, str) ) {
    stringstream ss; 
    ss << str; 
    ss >> ellipse_u>>ellipse_v>>ellipse_a>>ellipse_b>>ellipse_c;
    EllipseRegion tmp( ellipse_u, ellipse_v, ellipse_a, ellipse_b, ellipse_c);
    featureVec.push_back(tmp);   
  }
  ifs.close();
}

/*
read .txt file to vector<Mat>
*/
void read_file_to_affmatrix(cv::Mat& affMat, string filename){

  ifstream ifs( filename.c_str(),  std::ifstream::in); 
  if (! ifs.is_open() )
    cout<<"Cannot open "<<filename.c_str() <<endl;
  string str;
  int idx  = 0; 
  float a11, a12, a13, a21, a22, a23, a31, a32, a33 ;   
  while ( std::getline(ifs, str) ) {
    stringstream ss; 
    ss << str; 
    ss >>  a11 >> a12 >> a13 >> a21>> a22 >> a23 >> a31 >> a32 >> a33 ;   
    Mat data_vector = (Mat_<float>(1,9) << a11, a12, a13, a21, a22, a23, a31, a32, a33 );
    data_vector.copyTo(affMat.row(idx) ) ;
    idx++;    

  }
  ifs.close();
}

void read_file_to_errortable(cv::Mat& error_table, string filename){
   ifstream ifs( filename.c_str(),  std::ifstream::in); 
  if (! ifs.is_open() )
    cout<<"Cannot open "<<filename.c_str() <<endl;
  string str;
  int idx  = 0; 
  float val ; 
  int id1, id2;   
  while ( std::getline(ifs, str) ) {
    stringstream ss; 
    ss << str; 
    ss >>  val >> id1 >> id2 ;   
    Mat data_vector = (Mat_<float>(1,3) << val, id1, id2 );
    data_vector.copyTo(error_table.row(idx) ) ;
    idx++;    
  }
  ifs.close();
}




void dump_ellipse_to_file( vector<EllipseRegion>& featureVec, string filename){
  ofstream ofs(filename.c_str(), std::ofstream::out);
  for (auto v: featureVec){
    ofs << v.ellipse_u <<" "<<v.ellipse_v <<" "<<v.ellipse_a<<" "<<v.ellipse_b<<" "<<v.ellipse_c<<"\n";
  }
  ofs.close();
}

void check_max_min_value(const Mat& input_mat){
    double minVal, maxVal; 
    Point minLoc, maxLoc; 
    minMaxLoc(input_mat, &minVal, &maxVal, &minLoc, &maxLoc);
         cout<<"The mininum and maximum value of matrix is : ("<<minVal<<","<<maxVal<<")."<<endl;
}

/*  Convert openCV mat type to string */
string type2str(int type) {
  string r;
  uchar depth = type & CV_MAT_DEPTH_MASK;
  uchar chans = 1 + (type >> CV_CN_SHIFT);
  switch ( depth ) {
    case CV_8U:  r  = "8U"; break;
    case CV_8S:  r   = "8S"; break;
    case CV_16U: r = "16U"; break;
    case CV_16S: r  = "16S"; break;
    case CV_32S: r  = "32S"; break;
    case CV_32F: r  = "32F"; break;
    case CV_64F: r  = "64F"; break;
    default:     r      = "User"; break;
  }
  r += "C";
    r += (chans+'0');
    return r;
}

//void BruteRSearch(double *p, double *qp , double r, int N, int dim, vector<int>* idcv) {  //Ricerca bruta del nearest neighbour in 2D
void BruteRSearch(float *p, float *qp , float r, int N, int dim, vector<int>* idcv) {         
    int i, j;
    //double mindist=r*r;
    //double dist;
    float mindist = r*r;
    float dist;
    for (i=0;i<N*dim;i=i+dim) {
        dist=0;
        for(j=0;j<dim && dist<=mindist;j++) {
            dist=dist+(p[i+j]-qp[j])*(p[i+j]-qp[j]);
        }      
      if (dist<=mindist) { 
            idcv->push_back(i/dim);
        }
    }  
}